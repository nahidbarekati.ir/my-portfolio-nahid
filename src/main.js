import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueRouter from 'vue-router'

// Vue.use(VueRouter)
// const routers = new vueRouter({
//   router,
//   mode: 'history',
//   scrollBehavior(to , form , savedPosition) {
//     if(savedPosition){
      
//       return {savedPosition}
//     }
//     if(to.hash){
      
//       return {selector: to.hash}
//     }
//     return{x:0 , y:0}
//   }

// })


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
