import Vue from 'vue';
import Vuex from 'vuex';
import educations from './modules/educations';
import progressBars from './modules/progressBars'
import exprinces from './modules/exprinces'
import services from './modules/services'
import testimonials from './modules/testimonials'
import blogs from './modules/blogs'
import personalInfo from './modules/personalInfo'

//import * as types from "./mutation-types";

Vue.use(Vuex);

export default new Vuex.Store({
    modules : {
      educations,
      progressBars,
      exprinces,
      services,
      testimonials,
      blogs,
      personalInfo

    },
  
});
  



