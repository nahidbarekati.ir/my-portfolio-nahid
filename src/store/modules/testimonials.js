import testimonials from '../../data/testimonial'

const state = {
    testimonials : []
    
};

const getters = {
    allTestimonials: state => state.testimonials
        
};
const mutations = {
    setTestimonials(state, {testimonials}) {
        state.testimonials = testimonials;
    },
    
};

const actions = {
    getAllTestimonials ({ commit }) {
        testimonials.getTestimonials(testimonials => {
            commit('setTestimonials' , {testimonials})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}