import exprinces from '../../data/exprinces'

const state = {
    exprinces : []
    
};

const getters = {
    allExprinces: state => state.exprinces
        
};
const mutations = {
    setExprinces(state, {exprinces}) {
        state.exprinces = exprinces;
    },
    
};

const actions = {
    getAllExprinces({ commit }) {
        exprinces.getExprinces(exprinces => {
            commit('setExprinces' , {exprinces})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}