import blogs from '../../data/blogs'

const state = {
    blogs : []
    
};

const getters = {
    allBlogs: state => state.blogs
        
};
const mutations = {
    setBlogs(state, {blogs}) {
        state.blogs = blogs;
    },
    
};

const actions = {
    getAllBlogs ({ commit }) {
        blogs.getBlogs(blogs => {
            commit('setBlogs' , {blogs})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}