import personalInfo from '../../data/personalInfo'

const state = {
    personalInfo : []
    
};

const getters = {
    allPersonalInfo: state => state.personalInfo
        
};
const mutations = {
    setPersonalInfo(state, {personalInfo}) {
        state.personalInfo = personalInfo;
    },
    
};

const actions = {
    getAllPersonalInfo ({ commit }) {
        personalInfo.getPersonalInfo(personalInfo => {
            commit('setPersonalInfo' , {personalInfo})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}