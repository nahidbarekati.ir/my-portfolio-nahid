import services from '../../data/services'

const state = {
    services : []
    
};

const getters = {
    allServices: state => state.services
        
};
const mutations = {
    setServices(state, {services}) {
        state.services = services;
    },
    
};

const actions = {
    getAllServices ({ commit }) {
        services.getServices(services => {
            commit('setServices' , {services})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}