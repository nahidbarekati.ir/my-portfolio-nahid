import progressBars from '../../data/progressBars'

const state = {
    progressBars : []
    
};

const getters = {
    allProgressBars: state => state.progressBars
        
};
const mutations = {
    setProgressBars(state, {progressBars}) {
        state.progressBars = progressBars;
    },
    
};

const actions = {
    getAllProgressBars ({ commit }) {
        progressBars.getProgressBar(progressBars => {
            commit('setProgressBars' , {progressBars})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}