import educations from '../../data/educations'

const state = {
    educations : []
    
};

const getters = {
    allEducations: state => state.educations
        
};
const mutations = {
    setEducations(state, {educations}) {
        state.educations = educations;
    },
    
};

const actions = {
    getAllEducations ({ commit }) {
        educations.getEducations(educations => {
            commit('setEducations' , {educations})
        })
    }
  
}



export default {
    state,
    getters,
    actions,
    mutations
}