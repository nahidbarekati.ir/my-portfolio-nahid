import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/pages/Home.vue'
import Post from '../views/pages/Post.vue'


Vue.use(VueRouter)


const routes = [
  {path: '/', name : 'Home', component: Home },
  { path: '/about-me', name : 'Aboutme', component: Home },
  { path: '/portfilo', name : 'Progressbars', component: Home},
  { path: '/testimonial', name : 'Testimonial' , component: Home},
  { path: '/contact', name : 'Contact', component: Home },
  {  path: '/post/:id', name: 'post', component: Post},
  { path: '*', redirect :{name :'Home'} },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to , form , savedPosition) {
    if(savedPosition){
      
      return {savedPosition}
    }
    if(to.hash){
      
      return {selector: to.hash}
    }
    return{x:0 , y:0}
  }
})

export default router
